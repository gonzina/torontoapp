import React from "react";

import WelcomeScreen from "./app/screens/WelcomeScreen";
import RegisterScreen from "./app/screens/RegisterScreen";
import MenuScreen from "./app/screens/MenuScreen";
import AppointmentScreen from "./app/screens/AppointmentScreen";
import MyAppointmentScreen from "./app/screens/MyAppointmentScreen";
import ProfileScreen from "./app/screens/ProfileScreen";
import ShowroomScreen from "./app/screens/ShowroomScreen";
import ContactScreen from "./app/screens/ContactScreen";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";

const Stack = createStackNavigator();

const App = () => {
  return (
    // <WelcomeScreen />
    <NavigationContainer>
      <Stack.Navigator initialRouteName="WelcomeScreen">
        <Stack.Screen
          name="WelcomeScreen"
          component={WelcomeScreen}
          options={{ headerShown: false }}
        />
        <Stack.Screen
          name="RegisterScreen"
          component={RegisterScreen}
          options={{ headerShown: false }}
        />
        <Stack.Screen
          name="MenuScreen"
          component={MenuScreen}
          options={{ headerShown: false }}
        />
        <Stack.Screen
          name="AppointmentScreen"
          component={AppointmentScreen}
          options={{ headerShown: false }}
        />
        <Stack.Screen
          name="ContactScreen"
          component={ContactScreen}
          options={{ headerShown: false }}
        />
        <Stack.Screen
          name="MyAppointmentScreen"
          component={MyAppointmentScreen}
          options={{ headerShown: false }}
        />
        <Stack.Screen
          name="ProfileScreen"
          component={ProfileScreen}
          options={{ headerShown: false }}
        />
        <Stack.Screen
          name="ShowroomScreen"
          component={ShowroomScreen}
          options={{ headerShown: false }}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
}

export default App;
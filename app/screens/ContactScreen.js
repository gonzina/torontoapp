import React from "react";
import { FlatList } from "react-native";
import Screen from "../components/Screen";
import SocialCard from "../components/SocialCard";
import TextApp from "../components/TextApp";
import defaultStyles from "../config/defaultStyles";

// Facebook
// Instagram
// Whatsapp
// Direccion
const media = [
  { id: 1, icon: "facebook" },
  { id: 2, icon: "instagram" },
  { id: 3, icon: "whatsapp" },
  { id: 4, icon: "map-marker" },
];

function ContactScreen() {
  return (
    <Screen>
      <FlatList
        data={media}
        keyExtractor={(item) => item.id.toString()}
        renderItem={({ item }) => <SocialCard icon={item.icon} />}
        numColumns={2}
        contentContainerStyle={{ alignItems: "center" }}
        ListHeaderComponent={
          <TextApp style={defaultStyles.h1}>Contacto</TextApp>
        }
        ListHeaderComponentStyle={{ alignItems: "center" }}
      />
    </Screen>
  );
}

export default ContactScreen;

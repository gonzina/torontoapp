import React, { useState } from "react";

import Screen from "../components/Screen";
import { TextInput, Image, StyleSheet, View } from "react-native";
import InputTextApp from "../components/InputTextApp";
import ButtonApp from "../components/ButtonApp";

function RegisterScreen() {
  const [userName, setUserName] = useState("");
  const [password, setPassword] = useState("");

  return (
    <Screen style={styles.container}>
      <Image style={styles.logo} source={require("../assets/logo.png")} />

      <InputTextApp
        autoCapitalize="none"
        autoCorrect={false}
        icon="mail"
        onChangeText={(text) => setUserName(text)}
        keyboardType="email-address"
        placeholder="E-mail"
        textContentType="emailAddress"
      />
      <InputTextApp
        autoCapitalize="none"
        autoCorrect={false}
        icon="lock"
        onChangeText={(text) => setPassword(text)}
        placeholder="Contraseña"
        secureTextEntry
        textContentType="password"
      />
      <ButtonApp
        title="Registrarse"
        onPress={() => console.log(userName, password)}
      />
    </Screen>
  );
}

const styles = StyleSheet.create({
  container: {
    justifyContent: "flex-end",
    alignItems: "center",
    paddingBottom: 60,
  },
  form: {
    width: "100%",
    alignItems: "center",
    // justifyContent: "space-between",
  },
  logo: {
    width: 300,
    height: 300,
    // resizeMode: "contain",
    position: "absolute",
    top: "10%",
  },
});

export default RegisterScreen;

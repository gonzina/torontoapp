import React, { useState } from "react";
import { StyleSheet, View } from "react-native";
import ButtonApp from "../components/ButtonApp";
import InputTextApp from "../components/InputTextApp";
import Screen from "../components/Screen";
import TextApp from "../components/TextApp";
import defaultStyles from "../config/defaultStyles";
import firebaseService from "../../Service/firebase";
import * as firebase from "firebase";

const user = {
  name: "Esteban",
  lastname: "Lencina",
  phone: "3437402010",
  address: "25 de mayo 2816 Depto 5",
};

const ProfileScreen = (props) => {
  const [name, setName] = useState(props.route.params.userId);
  const [lastname, setLastname] = useState(props.route.params.userName);
  const [phone, setPhone] = useState(user.phone);
  const [address, setAddress] = useState(props.route.params.userEmail);

  const onPressLogout = async () => {
    try {
      await firebaseService.logout();
      props.navigation.navigate("WelcomeScreen");
      alert("Usted se deslogueo correctamente");
      console.log("LogOut");
    } catch (e) {
      alert("ERRORRRRRRR!!!!.");
      console.log("ERROR!");
    }
  };

  return (
    <Screen style={styles.container}>
      <View style={styles.section}>
        <TextApp style={defaultStyles.h1}>Mi Perfil</TextApp>
        <InputTextApp
          icon="user"
          onChangeText={(text) => setName(text)}
          autoCompleteType="name"
          placeholder="Nombre"
          value={name}
        />
        <InputTextApp
          icon="user"
          onChangeText={(text) => setLastname(text)}
          placeholder="Apellido"
          value={lastname}
        />
        <InputTextApp
          icon="phone"
          onChangeText={(text) => setPhone(text)}
          keyboardType="numeric"
          autoCompleteType="tel"
          placeholder="Teléfono"
          value={phone}
        />
        <InputTextApp
          icon="map"
          onChangeText={(text) => setAddress(text)}
          autoCompleteType="street-address"
          placeholder="Dirección"
          value={address}
        />
      </View>
      <View style={styles.section}>
        <ButtonApp title="Guardar cambios" />
        <ButtonApp
          title="Cerrar Sesión"
          color="secondary"
          onPress={onPressLogout}
        />
      </View>
    </Screen>
  );
}

const styles = StyleSheet.create({
  container: {
    alignItems: "center",
    justifyContent: "space-between",
  },
  section: {
    width: "100%",
    alignItems: "center",
  },
});
export default ProfileScreen;

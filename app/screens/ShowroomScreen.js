import React from "react";
import { FlatList, StyleSheet, View } from "react-native";
import ProductCard from "../components/ProductCard";
import Screen from "../components/Screen";
import TextApp from "../components/TextApp";
import defaultStyles from "../config/defaultStyles";

const products = [
  {
    id: 1,
    title: "Shampoo",
    img: require("../assets/img/product_1.jpg"),
  },
  {
    id: 2,
    title: "Pomada para barba",
    img: require("../assets/img/product_2.jpg"),
  },
  {
    id: 3,
    title: "Tónico para el cabello",
    img: require("../assets/img/product_3.jpg"),
  },
];

function ShowroomScreen() {
  return (
    <Screen style={styles.container}>
      <FlatList
        data={products}
        keyExtractor={(item) => item.id.toString()}
        renderItem={({ item }) => (
          <ProductCard
            title={item.title}
            subtitle={item.subtitle}
            img={item.img}
          />
        )}
        ListHeaderComponent={
          <TextApp style={defaultStyles.h1}>Productos</TextApp>
        }
        ListHeaderComponentStyle={{ alignItems: "center" }}
      />
    </Screen>
  );
}

const styles = StyleSheet.create({
  container: {
    // alignItems: "center",
  },
});

export default ShowroomScreen;

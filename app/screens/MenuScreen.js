import React from "react";
import { View, StyleSheet, Image } from "react-native";
import { FontAwesome5 } from "@expo/vector-icons";

import Screen from "../components/Screen";
import ButtonImg from "../components/ButtonImg";
import colors from "../config/colors";

const MenuScreen = (props) => {
  console.log(props.route.params.userId);
  console.log(props.route.params.userName);
  console.log(props.route.params.userEmail);
  return (
    <Screen style={styles.screen}>
      <View style={styles.header}>
        <Image
          style={styles.logo}
          source={require("../assets/logobasico.png")}
        />
        <FontAwesome5 name="bell" size={24} color={colors.white} />
      </View>
      <ButtonImg
        title="Reservar"
        img={require("../assets/img/boton_reservar.jpg")}
        icon={require("../assets/icons/razor.png")}
        onPress={() => props.navigation.navigate("AppointmentScreen", {userId: props.route.params.userId, userName: props.route.params.userName, userEmail: props.route.params.userEmail})}
      />
      <ButtonImg
        title="Mis Turnos"
        desc="Próximo Turno 31 de agosto a las 09:15hs"
        img={require("../assets/img/boton_misturnos.jpg")}
        icon={require("../assets/icons/clipboard.png")}
        onPress={() => props.navigation.navigate("MyAppointmentScreen")}
      />
      <View style={styles.buttonsColumn}>
        <ButtonImg
          title="Perfil"
          img={require("../assets/img/boton_miperfil.jpg")}
          icon={require("../assets/icons/user.png")}
          onPress={() => props.navigation.navigate("ProfileScreen", {userId: props.route.params.userId, userName: props.route.params.userName, userEmail: props.route.params.userEmail})}
        />
        <ButtonImg
          title="Showroom"
          img={require("../assets/img/boton_showroom.jpg")}
          icon={require("../assets/icons/shop.png")}
          onPress={() => props.navigation.navigate("ShowroomScreen")}
        />
      </View>
      <View style={styles.buttonsColumn}>
        <ButtonImg
          title="Cómo llegar"
          img={require("../assets/img/boton_comollegar.jpg")}
          icon={require("../assets/icons/location.png")}
          onPress={() => console.log("Se presiono el boton Como llegar")}
        />
        <ButtonImg
          title="Redes Sociales"
          img={require("../assets/img/boton_redessociales.jpg")}
          icon={require("../assets/icons/love.png")}
          onPress={() => props.navigation.navigate("ContactScreen")}
        />
      </View>
    </Screen>
  );
}

const styles = StyleSheet.create({
  screen: {
    flex: 1,
    paddingTop: 5,
  },
  buttonsColumn: {
    flex: 1,
    flexDirection: "row",
  },
  header: {
    flex: 0.3,
    flexDirection: "row",
    justifyContent: "space-around",
    alignItems: "center",
    paddingHorizontal: 20,
  },
  logo: {
    height: "70%",
    resizeMode: "contain",
  },
});

export default MenuScreen;

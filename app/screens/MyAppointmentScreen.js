import React from "react";
import { FlatList, StyleSheet, View } from "react-native";
import moment from "moment";

import Screen from "../components/Screen";
import Shift from "../components/Shift";
import TextApp from "../components/TextApp";
import defaultStyles from "../config/defaultStyles";

const appointments = [
  {
    id: 1,
    date: moment([2021, 9, 20]).hour(9).minutes(30),
    barber: "Matías",
    services: "Pelo y Barba",
  },
  // {
  //   id: 2,
  //   date: moment([2021, 9, 15]).hour(18).minutes(0),
  //   barber: "Juan",
  //   services: "Pelo",
  // },
  // {
  //   id: 3,
  //   date: moment([2020, 8, 24]).hour(17).minutes(30),
  //   barber: "Rodrigo",
  //   services: "Barba",
  // },
  // {
  //   id: 4,
  //   date: moment([2020, 8, 24]).hour(9).minutes(30),
  //   barber: "Matías",
  //   services: "Pelo y Barba",
  // },
  // {
  //   id: 5,
  //   date: moment([2020, 8, 15]).hour(15).minutes(0),
  //   barber: "Juan",
  //   services: "Pelo",
  // },
  // {
  //   id: 6,
  //   date: moment([2020, 11, 27]).hour(16).minutes(0),
  //   barber: "Rodrigo",
  //   services: "Barba",
  // },
];

function MyAppointmentScreen() {
  moment.locale("es");
  return (
    <Screen>
      <FlatList
        data={appointments}
        keyExtractor={(item) => item.id.toString()}
        renderItem={({ item }) => (
          <Shift
            date={item.date.format("DD/MM/YYYY")}
            hour={item.date.format("HH:mm")}
            barber={item.barber}
            services={item.services}
            active={moment() < item.date}
          />
        )}
        ListHeaderComponent={
          <TextApp style={defaultStyles.h1}>Mis Turnos</TextApp>
        }
        ListHeaderComponentStyle={{ alignItems: "center" }}
      />
    </Screen>
  );
}

export default MyAppointmentScreen;

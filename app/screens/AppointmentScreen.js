import React, { useState } from "react";
import { ScrollView } from "react-native";
import firebase from '../../Service/firebase'; 

import Screen from "../components/Screen";
import SelectItem from "../components/SelectItem";
import SelectDate from "../components/SelectDate";
import SelectHour from "../components/SelectHour";
import ButtonApp from "../components/ButtonApp";

const barbers = [
  {
    id: 0,
    name: "Juan",
    img: require("../assets/img/barber_1.jpg"),
  },
  {
    id: 1,
    name: "Pedro",
    img: require("../assets/img/barber_2.jpg"),
  },
  {
    id: 2,
    name: "Matías",
    img: require("../assets/img/barber_3.jpg"),
  },
  {
    id: 99,
    name: "Indistinto",
    iconNotSelected: require("../assets/icons/shuffle_white.png"),
    iconSelected: require("../assets/icons/shuffle_gold.png"),
  },
];

const services = [
  {
    id: 0,
    name: "Cabello",
    iconNotSelected: require("../assets/icons/hair_white.png"),
    iconSelected: require("../assets/icons/hair_gold.png"),
  },
  {
    id: 1,
    name: "Barba",
    iconNotSelected: require("../assets/icons/beard_white.png"),
    iconSelected: require("../assets/icons/beard_gold.png"),
  },
];

function AppointmentScreen(props) {
  const [hair, setHair] = useState(true);
  const [beard, setBeard] = useState(false);
  const [barber, setBarber] = useState([]);
  const [date, setDate] = useState("");
  const [hour, setHour] = useState("");
  const [saludo, setSaludo] = useState("Hola tebi");

  const showButton = (hair || beard) && barber && date !== "" && hour !== "";

  const saveTurno = () => {
    firebase.db.collection('turnos').add({
      id: props.route.params.userId,
      name: props.route.params.userName,
      pelo: hair,
      barba: beard,
      barbero: barber,
      fecha: date,
      hora: hour,
      saludo: saludo

    })
  }

  return (
    <Screen>
      <ScrollView>
        <SelectItem
          title="Seleccione Servicio"
          subtitle="(Puede seleccionar ambos)"
          items={services}
          itemState={[hair, beard]}
          onItemSelected={(id) => {
            id === 0 ? setHair(!hair) : setBeard(!beard);
          }}
          square={true}
          multiselec={true}
        />
        <SelectItem
          title="Seleccione Barbero"
          subtitle="(Desliza para ver todos los barberos)"
          items={barbers}
          itemState={barber}
          onItemSelected={(newBarber) => setBarber(newBarber)}
        />
        <SelectDate date={date} onDateSelected={(newDay) => setDate(newDay)} />
        {date !== "" && (
          <SelectHour
            hour={hour}
            onHourSelected={(newHour) => setHour(newHour)}
          />
        )}
        {showButton && <ButtonApp title="Reservar Turno" onPress={() => saveTurno()}/>}
      </ScrollView>
    </Screen>
  );
}

export default AppointmentScreen;

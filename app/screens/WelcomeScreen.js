import React, { useState } from "react";
import {
  ImageBackground,
  StyleSheet,
  StatusBar,
  Image,
  View,
} from "react-native";
import * as firebase from "firebase";
import { firebaseService } from "../../Service/firebase";
import * as Facebook from "expo-facebook";
import * as Google from "expo-google-app-auth";


import Screen from "../components/Screen";
import ButtonApp from "../components/ButtonApp";
import InputTextApp from "../components/InputTextApp";
import ButtonMini from "../components/ButtonMini";
import TextApp from "../components/TextApp";
import ButtonIcon from "../components/ButtonIcon";

const WelcomeScreen = (props) => {
  const [userName, setName] = useState("credential.userName");
  // const [password, setPassword] = useState("");
  const [loggedIn, setloggedIn] = useState(false);
  const [userInfo, setuserInfo] = useState([]);
  // state = { user: null };

  // // componentDidMount() {
  // //   this.initAsync();
  // // }

  // initAsync = async () => {
  //   await GoogleSignIn.initAsync({
  //     // You may ommit the clientId when the firebase `googleServicesFile` is configured
  //     androidClientId: '1068140055851-cljk08g1q0qr1jss7m62vtiau717ta96.apps.googleusercontent.com',
  //     iosClientId: '1068140055851-vtebta49f3rf70ebs4hlb8bhiijtibtp.apps.googleusercontent.com',
  //   });
  //   this._syncUserWithStateAsync();
  // };

  // _syncUserWithStateAsync = async () => {
  //   const user = await GoogleSignIn.signInSilentlyAsync();
  //   this.setState({ user });
  // };

  // signOutAsync = async () => {
  //   await GoogleSignIn.signOutAsync();
  //   this.setState({ user: null });
  // };

  // signInAsync = async () => {
  //   try {
  //     await GoogleSignIn.askForPlayServicesAsync();
  //     const { type, user } = await GoogleSignIn.signInAsync();
  //     if (type === 'success') {
  //       this._syncUserWithStateAsync();
  //     }
  //   } catch ({ message }) {
  //     alert('login: Error:' + message);
  //   }
  // };

  // onPress = () => {
  //   if (this.state.user) {
  //     this.signOutAsync();
  //   } else {
  //     this.signInAsync();
  //   }
  // };

  // try {
  //   await GoogleSignIn.initAsync({
  //     // You may ommit the clientId when the firebase `googleServicesFile` is configured
  //     androidClientId: '1068140055851-cljk08g1q0qr1jss7m62vtiau717ta96.apps.googleusercontent.com',
  //     iosClientId: '1068140055851-vtebta49f3rf70ebs4hlb8bhiijtibtp.apps.googleusercontent.com',
  //     scopes: ['profile', 'email'],
  //     // Provide other custom options...
  //   });
  // } catch ({ message }) {
  //   alert('GoogleSignIn.initAsync(): ' + message);
  // }
  signInGoogle = async () => {
    try {
      const result = await Google.logInAsync({
        androidClientId:
          "1068140055851-cljk08g1q0qr1jss7m62vtiau717ta96.apps.googleusercontent.com",
        iosClientId:
          "1068140055851-vtebta49f3rf70ebs4hlb8bhiijtibtp.apps.googleusercontent.com",
        scopes: ["profile", "email"],
      });

      if (result.type === "success") {
        props.navigation.navigate("MenuScreen", {userId: result.user.id, userName: result.user.name, userEmail: result.user.email});
        //console.log(result.user.id);
        return result.accessToken;
      } else {
        return { cancelled: true };
      }
    } catch (e) {
      return { error: true };
    }
  };

  signInFB = async () => {
    await Facebook.initializeAsync("803132440423698");
    const {
      type,
      token,
      expires,
      permissions,
      declinedPermissions,
    } = await Facebook.logInWithReadPermissionsAsync({
      permissions: ["public_profile"],
    });
    if (type === "success") {
      // Get the user's name using Facebook's Graph API
      const response = await fetch(
        `https://graph.facebook.com/me?access_token=${token}`
      );
      //alert("Logged in!", `Hi ${(await response.json()).name}!`);
      this.setName = (await response.json()).name;
      console.log(this.setName);
      const credential = firebase.auth.FacebookAuthProvider.credential(token);
      firebase.auth.Auth.Persistence.LOCAL;
      var user = firebase.auth().currentUser;
      if(user == null){
        alert("USUARIO NULO!!!");
      }
      console.log(user.displayName);
      console.log(user.email);
      console.log(user.emailVerified);
      console.log(user.uid);
      console.log(user.phoneNumber);
      props.navigation.navigate("MenuScreen", {userId: user.uid, userName: user.displayName, userEmail: user.email});
      //console.log(awaitresponse.json().name);

      firebase
        .auth()
        .signInWithCredential(credential)
        .catch((error) => {
          console.log(error);
        });
    }
  };

  // _signIn = async () => {
  //   try {
  //     await GoogleSignin.hasPlayServices();
  //     const { accessToken, idToken } = await GoogleSignin.signIn();
  //     setloggedIn(true);
  //   } catch (error) {
  //     if (error.code === statusCodes.SIGN_IN_CANCELLED) {
  //       // user cancelled the login flow
  //       alert("Cancel");
  //     } else if (error.code === statusCodes.IN_PROGRESS) {
  //       alert("Signin in progress");
  //       // operation (f.e. sign in) is in progress already
  //     } else if (error.code === statusCodes.PLAY_SERVICES_NOT_AVAILABLE) {
  //       alert("PLAY_SERVICES_NOT_AVAILABLE");
  //       // play services not available or outdated
  //     } else {
  //       // some other error happened
  //     }
  //   }
  // };

  // useEffect(() => {
  //   GoogleSignin.configure({
  //     scopes: ["email"], // what API you want to access on behalf of the user, default is email and profile
  //     webClientId:
  //       "418977770929-g9ou7r9eva1u78a3anassxxxxxxx.apps.googleusercontent.com", // client ID of type WEB for your server (needed to verify user ID and offline access)
  //     offlineAccess: true, // if you want to access Google API on behalf of the user FROM YOUR SERVER
  //   });
  // }, []);

  // signOut = async () => {
  //   try {
  //     await GoogleSignin.revokeAccess();
  //     await GoogleSignin.signOut();
  //     setloggedIn(false);
  //     setuserInfo([]);
  //   } catch (error) {
  //     console.error(error);
  //   }
  // };

  // signInGoogle = async () => {
  //   const result = await firebase
  //     .auth()
  //     .getRedirectResult({
  //       androidClientId:
  //         "1068140055851-cljk08g1q0qr1jss7m62vtiau717ta96.apps.googleusercontent.com",
  //       iosClientId:
  //         "1068140055851-vtebta49f3rf70ebs4hlb8bhiijtibtp.apps.googleusercontent.com",
  //       scopes: ["profile", "email"],
  //     })
  //     .then(function (result) {
  //       if (result.credential) {
  //         // This gives you a Google Access Token.
  //         var token = result.credential.accessToken;
  //       }
  //       var user = result.user;
  //     });

  //   // Start a sign in process for an unauthenticated user.
  //   var provider = new firebase.auth.GoogleAuthProvider();
  //   provider.addScope("profile");
  //   provider.addScope("email");
  //   firebase.auth().signInWithRedirect(provider);
  // };

  // signInGoogle = async () => {
  //   try {
  //     const result = await Google.logInAsync({
  //       androidClientId:
  //         "1068140055851-cljk08g1q0qr1jss7m62vtiau717ta96.apps.googleusercontent.com",
  //       iosClientId:
  //         "1068140055851-vtebta49f3rf70ebs4hlb8bhiijtibtp.apps.googleusercontent.com",
  //       scopes: ["profile", "email"],
  //     });

  //     if (result.type === "success") {
  //       navigation.navigate("MenuScreen");
  //       return result.accessToken;
  //     } else {
  //       return { cancelled: true };
  //     }
  //   } catch (e) {
  //     return { error: true };
  //   }
  // };
  // googleSignIn = async () => {
  //   firebase
  //     .auth()
  //     .getRedirectResult()
  //     .then(function (result) {
  //       if (result.credential) {
  //         // This gives you a Google Access Token.
  //         var token = result.credential.accessToken;
  //       }
  //       var user = result.user;
  //     });

  //   // Start a sign in process for an unauthenticated user.
  //   var provider = new firebase.auth.GoogleAuthProvider();
  //   provider.addScope("profile");
  //   provider.addScope("email");
  //   firebase.auth().signInWithRedirect(provider);
  // };

  return (
    <ImageBackground
      style={styles.container}
      source={require("../assets/img/welcome.jpg")}
    >
      <StatusBar style="auto" />
      <Image style={styles.logo} source={require("../assets/logo.png")} />
      <TextApp style={styles.title}>Inicia sesión con:</TextApp>
      <ButtonIcon
        icon="apple"
        title="IOS"
        onPress={() => console.log("Apple")}
      />
      {/* <GoogleSigninButton
        style={{ width: 192, height: 48 }}
        size={GoogleSigninButton.Size.Wide}
        color={GoogleSigninButton.Color.Dark}
        onPress={this._signIn}
      /> */}
      <ButtonIcon icon="google" title="Google" onPress={this.signInGoogle} />
    </ImageBackground>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingBottom: 15,
    alignItems: "center",
    justifyContent: "flex-end",
  },
  buttons: {
    flexDirection: "row",
  },
  logo: {
    width:  "100%",
    height: "50%",
    // resizeMode: "contain",
    position: "absolute",
    top: "10%",
  },
  title: {
    fontSize: 30,
  },
});

export default WelcomeScreen;

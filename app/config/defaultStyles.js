import colors from "./colors";

export default {
  colors,
  buttonBorder: {
    borderRadius: 30,
    borderWidth: 3,
    borderColor: colors.white,
  },
  h1: {
    fontSize: 35,
    textTransform: "uppercase",
    marginBottom: 15,
  },
  verticalSeparation: {
    marginBottom: 30,
  },
};

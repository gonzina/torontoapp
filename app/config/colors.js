export default {
  primary: "goldenrod",
  white: "whitesmoke",
  gray: "darkgray",
  background: "#423f40",
  danger: "orangered",
};

import React from "react";
import { Image, StyleSheet, View } from "react-native";
import colors from "../config/colors";
import defaultStyles from "../config/defaultStyles";
import TextApp from "./TextApp";

function ProductCard(props) {
  return (
    <View style={[defaultStyles.buttonBorder, styles.container]}>
      <Image style={styles.img} source={props.img} />
      <View style={styles.text}>
        <TextApp style={styles.title}>{props.title}</TextApp>
        {props.subtitle && (
          <TextApp style={styles.subtitle}>{props.subtitle}</TextApp>
        )}
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    marginHorizontal: 10,
    marginVertical: 5,
    // width: "100%",
    overflow: "hidden",
    borderWidth: 0,
    backgroundColor: colors.primary,
  },
  img: {
    width: "100%",
    height: 350,
  },
  text: {
    marginLeft: 30,
    paddingVertical: 10,
  },
  title: {
    fontSize: 25,
    textTransform: "uppercase",
  },
  subtitle: {
    fontSize: 20,
  },
});

export default ProductCard;

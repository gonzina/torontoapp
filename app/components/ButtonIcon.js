import React from "react";
import { StyleSheet, TouchableOpacity, View } from "react-native";
import defaultStyles from "../config/defaultStyles";
import { MaterialCommunityIcons } from "@expo/vector-icons";
import TextApp from "./TextApp";

function ButtonIcon(props) {
  return (
    <TouchableOpacity style={styles.container} onPress={props.onPress}>
      <View style={[defaultStyles.buttonBorder, styles.button]}>
        <MaterialCommunityIcons
          name={props.icon}
          size={24}
          color={defaultStyles.colors.white}
        />
        <TextApp style={styles.title}>{props.title}</TextApp>
      </View>
    </TouchableOpacity>
  );
}

const styles = StyleSheet.create({
  button: {
    margin: 3,
    width: "50%",
    height: 70,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: defaultStyles.colors.primary,
    flexDirection: "row",
  },
  container: {
    width: "100%",
    alignItems: "center",
  },
  title: {
    fontSize: 25,
    paddingLeft: 10,
  },
});

export default ButtonIcon;

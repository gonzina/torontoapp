import React from "react";
import {
  StyleSheet,
  ImageBackground,
  TouchableWithoutFeedback,
  Image,
  View,
} from "react-native";

import TextApp from "./TextApp";
import defaultStyles from "../config/defaultStyles";

function CheckboxItem(props) {
  const content = (
    <>
      <Image source={props.icon} style={styles.icon} />
      <TextApp style={props.selected ? styles.selected : styles.noSelected}>
        {props.name}
      </TextApp>
    </>
  );
  const allStyles = [
    defaultStyles.buttonBorder,
    styles.container,
    props.selected ? styles.selected : styles.noSelected,
    props.square && styles.square,
  ];
  return (
    <TouchableWithoutFeedback onPress={props.onPress}>
      {props.img ? (
        <ImageBackground style={allStyles} source={props.img}>
          {content}
        </ImageBackground>
      ) : (
        <View style={allStyles}>{content}</View>
      )}
    </TouchableWithoutFeedback>
  );
}

const styles = StyleSheet.create({
  container: {
    alignItems: "center",
    overflow: "hidden",
    paddingBottom: 5,
    justifyContent: "flex-end",
    width: 120,
    height: 170,
  },
  square: {
    height: 120,
  },
  icon: {
    paddingBottom: 5,
    height: 50,
    resizeMode: "contain",
    position: "absolute",
    top: "30%",
  },
  selected: {
    color: defaultStyles.colors.primary,
    borderColor: defaultStyles.colors.primary,
  },
  noSelected: {
    borderColor: defaultStyles.colors.white,
  },
});

export default CheckboxItem;

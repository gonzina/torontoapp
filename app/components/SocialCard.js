import React from "react";
import { StyleSheet, View } from "react-native";
import { MaterialCommunityIcons } from "@expo/vector-icons";

import defaultStyles from "../config/defaultStyles";
import TextApp from "./TextApp";

function SocialCard(props) {
  return (
    <View style={[defaultStyles.buttonBorder, styles.container]}>
      <MaterialCommunityIcons
        name={props.icon}
        size={50}
        color={defaultStyles.colors.white}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    margin: 10,
    height: 120,
    width: 120,
    alignItems: "center",
    justifyContent: "center",
  },
});

export default SocialCard;

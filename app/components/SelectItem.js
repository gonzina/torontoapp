import React from "react";
import { StyleSheet, View, FlatList } from "react-native";

import CheckboxItem from "./CheckboxItem";
import defaultStyles from "../config/defaultStyles";
import TextApp from "./TextApp";

function SelectItem({
  title,
  subtitle,
  items,
  itemState,
  onItemSelected,
  multiselec = false,
  square = false,
}) {
  const list = multiselec
    ? ({ item }) => (
        <CheckboxItem
          name={item.name}
          img={item.img}
          icon={itemState[item.id] ? item.iconSelected : item.iconNotSelected}
          square={square}
          selected={itemState[item.id]}
          onPress={() => onItemSelected(item.id)}
        />
      )
    : ({ item }) => (
        <CheckboxItem
          name={item.name}
          img={item.img}
          icon={
            itemState.id === item.id ? item.iconSelected : item.iconNotSelected
          }
          square={square}
          selected={itemState.id === item.id}
          onPress={() => onItemSelected(item)}
        />
      );

  return (
    <View style={[defaultStyles.verticalSeparation, styles.container]}>
      <TextApp style={[defaultStyles.h1, styles.title]}>{title}</TextApp>
      <TextApp style={styles.subtitle}>{subtitle}</TextApp>
      <FlatList
        data={items}
        keyExtractor={(item) => item.id.toString()}
        renderItem={list}
        horizontal={true}
        ItemSeparatorComponent={() => <View style={styles.itemSeparator} />}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
  title: {
    marginBottom: 0,
  },
  subtitle: {
    color: defaultStyles.colors.primary,
    marginBottom: 15,
    fontSize: 15,
  },
  itemSeparator: {
    width: 7,
    // height: 5,
  },
});

export default SelectItem;

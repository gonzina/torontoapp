import React from "react";
import {
  View,
  StyleSheet,
  ImageBackground,
  ImagePropTypes,
  Image,
  TouchableOpacity,
} from "react-native";
import defaultStyles from "../config/defaultStyles";
import TextApp from "./TextApp";

function ButtonImg(props) {
  return (
    <TouchableOpacity style={styles.container} onPress={props.onPress}>
      <ImageBackground
        style={[defaultStyles.buttonBorder, styles.button]}
        source={props.img}
        blurRadius={2}
      >
        {props.icon && <Image style={styles.icon} source={props.icon} />}
        <TextApp style={styles.title}>{props.title}</TextApp>
        {props.desc && <TextApp>{props.desc}</TextApp>}
      </ImageBackground>
    </TouchableOpacity>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  button: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    overflow: "hidden",
    borderColor: defaultStyles.colors.white,
    margin: 5,
  },
  icon: {
    height: 35,
    resizeMode: "contain",
    marginBottom: 5,
  },
  title: {
    fontSize: 30,
    textTransform: "uppercase",
  },
});

export default ButtonImg;

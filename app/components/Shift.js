import React from "react";
import { StyleSheet, View } from "react-native";
import { Feather } from "@expo/vector-icons";

import TextApp from "./TextApp";
import defaultStyles from "../config/defaultStyles";

function Shift({ date, hour, barber, services, active = true }) {
  return (
    <View style={styles.container}>
      <View
        style={[
          defaultStyles.buttonBorder,
          styles.box,
          active ? styles.colorActive : styles.colorInactive,
        ]}
      >
        <View style={styles.columns}>
          <Feather
            name="calendar"
            size={20}
            color={defaultStyles.colors.white}
          />
          <TextApp style={styles.text}>{date}</TextApp>
        </View>
        <View style={styles.columns}>
          <Feather name="clock" size={20} color={defaultStyles.colors.white} />
          <TextApp style={styles.text}>{hour}</TextApp>
        </View>
        <View style={styles.columns}>
          <Feather name="user" size={20} color={defaultStyles.colors.white} />
          <TextApp style={styles.text}>{barber}</TextApp>
        </View>
        <View style={styles.columns}>
          <Feather
            name="clipboard"
            size={20}
            color={defaultStyles.colors.white}
          />
          <TextApp style={styles.text}>{services}</TextApp>
        </View>
      </View>
      {active && (
        <Feather name="trash-2" size={20} color={defaultStyles.colors.danger} />
      )}
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    justifyContent: "flex-start",
    width: "100%",
    marginLeft: 50,
    alignItems: "center",
    flexDirection: "row",
  },
  box: {
    paddingVertical: 10,
    paddingHorizontal: 20,
    margin: 10,
    width: "70%",
  },
  colorActive: {
    backgroundColor: defaultStyles.colors.primary,
    borderWidth: 0,
  },
  colorInactive: {
    borderColor: defaultStyles.colors.white,
  },
  columns: {
    flexDirection: "row",
    justifyContent: "flex-start",
    alignItems: "center",
  },
  text: {
    marginLeft: 10,
    fontSize: 20,
  },
});

export default Shift;

import React from "react";
import { StyleSheet, SafeAreaView, StatusBar } from "react-native";
import Constants from "expo-constants";

import colors from "../config/colors";

function Screen(props) {
  return (
    <SafeAreaView style={[styles.container, props.style]}>
      <StatusBar style="light" />
      {props.children}
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    paddingTop: Constants.statusBarHeight,
    backgroundColor: colors.background,
    flex: 1,
  },
});

export default Screen;

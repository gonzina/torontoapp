import React, { useState } from "react";
import { Text, StyleSheet } from "react-native";
import { AppLoading } from "expo";
import * as Font from "expo-font";

import colors from "../config/colors";

const fetchFonts = () => {
  return Font.loadAsync({
    "din-condensed-bold": require("../assets/fonts/din-condensed-bold.ttf"),
    "Sign-Painter-Regular": require("../assets/fonts/Sign-Painter-Regular.ttf"),
    "signpainter-housescript": require("../assets/fonts/signpainter-housescript.ttf"),
  });
};

function TextApp({ children, style, font = "primary" }) {
  const [dataLoaded, setDataLoaded] = useState(false);

  if (!dataLoaded) {
    return (
      <AppLoading
        startAsync={fetchFonts}
        onFinish={() => {
          setDataLoaded(true);
        }}
      />
    );
  }

  return (
    <Text
      style={[
        styles.text,
        font === "primary" ? styles.primaryFont : styles.secondaryFont,
        style,
      ]}
    >
      {children}
    </Text>
  );
}

const styles = StyleSheet.create({
  text: {
    color: colors.white,
  },
  primaryFont: {
    fontFamily: "din-condensed-bold",
  },
  secondaryFont: {
    fontFamily: "signpainter-housescript",
  },
});

export default TextApp;

import React, { useState } from "react";
import { StyleSheet, View } from "react-native";
import { Calendar } from "react-native-calendars";
import { LocaleConfig } from "react-native-calendars";
import moment from "moment";
import { AntDesign } from "@expo/vector-icons";

import defaultStyles from "../config/defaultStyles";
import TextApp from "./TextApp";

LocaleConfig.locales["es"] = {
  monthNames: [
    "Enero",
    "Febrero",
    "Marzo",
    "Abril",
    "Mayo",
    "Junio",
    "Julio",
    "Agosto",
    "Septiembre",
    "Octubre",
    "Noviembre",
    "Diciembre",
  ],
  monthNamesShort: [
    "Ene.",
    "Feb.",
    "Mar.",
    "Abr.",
    "May.",
    "Jun.",
    "Jul.",
    "Ago.",
    "Sep.",
    "Oct.",
    "Nov.",
    "Dic.",
  ],
  dayNames: [
    "Domingo",
    "Lunes",
    "Martes",
    "Miércoles",
    "Jueves",
    "Viernes",
    "Sábado",
  ],
  dayNamesShort: ["Dom.", "Lun.", "Mar.", "Mie.", "Jue.", "Vie.", "Sab."],
  today: "Hoy",
};
LocaleConfig.defaultLocale = "es";

function SelectDate(props) {
  const [currentMonth, setCurrentMonth] = useState(moment().month() + 1);
  const [currentYear, setCurrentYear] = useState(moment().year());

  const disableWeekend = (month, year) => {
    const disabledDays = {};
    let i = 1;
    let numberInMonth = moment(
      "" + year + "-" + month,
      "YYYY-MM"
    ).daysInMonth();
    while (i <= numberInMonth) {
      let startDay = moment([year, month - 1, i]);
      if (startDay.day() === 0 || startDay.day() === 6) {
        disabledDays[startDay.format("YYYY-MM-DD")] = {
          disabled: true,
          disableTouchEvent: true,
        };
      }
      i++;
    }
    return disabledDays;
  };

  let minDate = moment().format("YYYY-MM-DD");
  let maxDate = moment().add(1, "month").format("YYYY-MM-DD");

  return (
    <View style={[defaultStyles.verticalSeparation, styles.container]}>
      <TextApp style={[defaultStyles.h1, styles.title]}>
        Seleccione fecha
      </TextApp>
      <Calendar
        renderArrow={(direction) => (
          <AntDesign
            name={direction}
            size={15}
            color={defaultStyles.colors.primary}
          />
        )}
        minDate={minDate}
        maxDate={maxDate}
        monthFormat={"MMMM yyyy"}
        onDayPress={(day) => {
          props.onDateSelected(day);
          console.log(day);
        }}
        onMonthChange={(day) => {
          setCurrentMonth(day.month);
          setCurrentYear(day.year);
        }}
        theme={{
          calendarBackground: defaultStyles.colors.background,
          textSectionTitleColor: defaultStyles.colors.white,
          todayTextColor: defaultStyles.colors.primary,
          dayTextColor: defaultStyles.colors.white,
          textDisabledColor: defaultStyles.colors.gray,
          arrowColor: defaultStyles.colors.primary,
          monthTextColor: defaultStyles.colors.primary,
        }}
        markedDates={{
          [props.date.dateString]: {
            selected: true,
            selectedColor: defaultStyles.colors.primary,
          },
          ...disableWeekend(currentMonth, currentYear),
        }}
      />
    </View>
  );
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "flex-start",
    marginVertical: 20,
  },
  title: {
    alignSelf: "center",
  },
});
export default SelectDate;

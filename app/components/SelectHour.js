import React, { useEffect } from 'react';
import moment from "moment";
import { View, FlatList, StyleSheet, Text } from "react-native";

import Item from "./Item";
import TextApp from "./TextApp";
import defaultStyles from "../config/defaultStyles";


function calculateHours(startHour, endHour) {
  let hoursArray = [];
  let h = moment(startHour, "hmm");
  while (h < moment(endHour, "hmm")) {
    hoursArray.push(h.format("HH:mm"));
    h.add(30, "minutes");
  }
  return hoursArray;
}

const hours = calculateHours(800, 2000);
const i = hours.length/3
const hours_3 = hours
const hours_2 = hours_3.splice(0,i*2)
const hours_1 = hours_2.splice(0,i)

function SelectHour(props) {

  const hoursColumn = (hours_column)=>(
    <View style={styles.column}>
      {hours_column.map(hours_column=>
          <Item
            key={hours_column}
            title={hours_column}
            selected={hours_column === props.hour}
            onPress={() => props.onHourSelected(hours_column)}
        />
      )}
    </View>
  ) 

  return (
    <View style={styles.container}>
      <TextApp style={defaultStyles.h1}>Seleccione Horario</TextApp>
      <View style={styles.hours}>
        {hoursColumn(hours_1)}
        {hoursColumn(hours_2)}
        {hoursColumn(hours_3)}
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    // flex: 1,
    width: "100%",
    alignItems: "center",
  },
  hours: {
    flexDirection:"row",
    justifyContent: "space-around",
    width: "100%",
    marginVertical: 15,
  },
  column: {
    alignItems: "center",
    width: "100%",
  },
});

export default SelectHour;

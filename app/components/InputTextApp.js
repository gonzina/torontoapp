import React from "react";
import { TextInput, View, StyleSheet } from "react-native";
import { Feather } from "@expo/vector-icons";

import colors from "../config/colors";

function InputTextApp({ icon, ...otherProps }) {
  return (
    <View style={styles.container}>
      {icon && (
        <Feather
          style={styles.icon}
          name={icon}
          size={20}
          color={colors.background}
        />
      )}
      <TextInput
        style={styles.textInput}
        placeholderTextColor={colors.gray}
        {...otherProps}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: colors.white,
    flexDirection: "row",
    width: "80%",
    alignItems: "center",
    borderRadius: 30,
    padding: 15,
    marginBottom: 20,
  },
  icon: {
    marginRight: 10,
  },
  textInput: {
    color: colors.background,
    fontSize: 20,
    borderColor: "blue",
  },
});

export default InputTextApp;

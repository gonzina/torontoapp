import React from "react";
import TextApp from "./TextApp";
import { StyleSheet, TouchableOpacity } from "react-native";

import colors from "../config/colors";

function ButtonMini({ color = "primary", onPress, title }) {
  return (
    <TouchableOpacity onPress={onPress}>
      <TextApp
        style={[
          title,
          color === "primary" ? styles.primaryColor : styles.secondaryColor,
        ]}
      >
        {title}
      </TextApp>
    </TouchableOpacity>
  );
}

const styles = StyleSheet.create({
  title: {
    fontSize: 25,
  },
  primaryColor: {
    color: colors.primary,
  },
  secondaryColor: {
    color: colors.white,
  },
});

export default ButtonMini;

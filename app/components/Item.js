import React from "react";
import { View, StyleSheet, TouchableOpacity } from "react-native";
import AppText from "./TextApp";

import defaultStyles from "../config/defaultStyles";

function Item({ selected, onPress, title }) {
  return (
    <TouchableOpacity style={styles.container} onPress={onPress}>
      <View
        style={[
          styles.button,
          defaultStyles.buttonBorder,
          selected ? styles.colorSelected : styles.colorNotSelected,
        ]}
      >
        <AppText style={styles.title}>{title}</AppText>
      </View>
    </TouchableOpacity>
  );
}

const styles = StyleSheet.create({
  container: {
    width: "30%",
    justifyContent: "center",
    alignItems: "center",
  },
  button: {
    justifyContent: "center",
    alignItems: "center",
    width: "80%",
    height: 50,
    marginBottom: 10,
  },
  colorSelected: {
    borderWidth: 0,
    backgroundColor: defaultStyles.colors.primary,
  },
  colorNotSelected: {
    borderColor: defaultStyles.colors.white,
    borderWidth: 1,
  },
  title: {
    color: defaultStyles.colors.white,
    fontSize: 20,
    textTransform: "uppercase",
  },
});

export default Item;

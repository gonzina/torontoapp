import React from "react";
import { View, StyleSheet, TouchableOpacity } from "react-native";
import AppText from "./TextApp";

import defaultStyles from "../config/defaultStyles";

function ButtonApp({ color = "principal", onPress, title }) {
  return (
    <TouchableOpacity style={styles.container} onPress={onPress}>
      <View
        style={[
          styles.button,
          defaultStyles.buttonBorder,
          color === "principal" ? styles.colorPrincipal : styles.colorSecondary,
        ]}
      >
        <AppText style={styles.title}>{title}</AppText>
      </View>
    </TouchableOpacity>
  );
}

const styles = StyleSheet.create({
  container: {
    width: "100%",
    justifyContent: "center",
    alignItems: "center",
  },
  button: {
    justifyContent: "center",
    alignItems: "center",
    width: "80%",
    height: 70,
    marginBottom: 20,
  },
  colorPrincipal: {
    borderWidth: 0,
    backgroundColor: defaultStyles.colors.primary,
    borderColor: defaultStyles.colors.primary,
  },
  colorSecondary: {
    borderColor: defaultStyles.colors.white,
  },
  title: {
    color: defaultStyles.colors.white,
    fontSize: 25,
    textTransform: "uppercase",
  },
});

export default ButtonApp;

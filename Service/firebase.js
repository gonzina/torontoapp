import * as firebase from "firebase";
import "firebase/firebase-firestore";

const firebaseConfig = {
  apiKey: "AIzaSyBv_LCZ5p-lxW6QISHlsgMDa6TimuXDcyQ",
  authDomain: "torontoapp-1e8bf.firebaseapp.com",
  databaseURL: "https://torontoapp-1e8bf.firebaseio.com",
  projectId: "torontoapp-1e8bf",
  storageBucket: "torontoapp-1e8bf.appspot.com",
  messagingSenderId: "117095396714",
  appId: "1:117095396714:web:bd584f6439ae5be86c6113",
  measurementId: "G-ZX53RCHHPL",
};

class Firebase {
  constructor() {
    firebase.initializeApp(firebaseConfig);
    this.auth = firebase.auth();
  }

  login = (email, pass) => {
    return this.auth.signInWithEmailAndPassword(email, pass);
  };

  createUser = (user, pass) => {
    return this.auth.createUserWithEmailAndPassword(user, pass);
  };

  logout = () => {
    return this.auth.signOut();
  };
}

const firebaseService = new Firebase();

const db = firebase.firestore();

export default {
  firebaseService,
  db,
} 
